#pragma once

#include "HeaderSerializer.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class HeaderSerializerMock : public ISerializer
{
public:
    MOCK_METHOD(bool, makeDirectHeader, (std::vector<uint8_t>& serializedHeader, const std::string& sender,const std::string& receiver), (override));
    MOCK_METHOD(bool, makeBroadcastHeader, (std::vector<uint8_t>& serializedHeader, const std::string& sender), (override));
    MOCK_METHOD(bool, makeLoginHeader, (std::vector<uint8_t>& serializedMsgOut, const std::string& login, const std::string& oldLogin), (override));
    MOCK_METHOD(bool, makeKeepAliveHeader, (std::vector<uint8_t>& serializedHeader), (override));
    MOCK_METHOD(bool, makeUserConnectionStateChangedHeader,(std::vector<uint8_t>& serializedMsgOut, const std::string& username,bool isConnected),(override));
    MOCK_METHOD(bool, makeConnectedUsersHeader,(std::vector<uint8_t>& serializedMsgOut, const std::string& userList),(override));
    MOCK_METHOD(bool, makeServerLogHeader,(std::vector<uint8_t>& serializedMsgOut, const std::string& logs),(override));

    MOCK_METHOD0(Die, void());
    virtual ~HeaderSerializerMock() { Die(); }
};
