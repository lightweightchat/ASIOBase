#pragma once

#include "IAsioContext.h"
#include <functional>

using ReadCallback = std::function<void(const asio::error_code &ce, const size_t)>;
using WriteCallback = std::function<void(const asio::error_code &ce, const size_t)>;
using ConnectCallback = std::function<void(const asio::error_code &ec)>;

class IAsioSocket
{
public:
    using ptr = std::shared_ptr<IAsioSocket>;
    
    IAsioSocket() = default;
    virtual ~IAsioSocket() = default;

    virtual void close() = 0;
    virtual void connect(const asio::ip::tcp::endpoint &endpoint, ConnectCallback callback) = 0;
    virtual void connect(const asio::ip::tcp::resolver::results_type &endpoint, ConnectCallback callback) = 0;

    virtual void read(uint8_t *buffer, size_t maxLength, ReadCallback callback) = 0;
    virtual void readUntil(asio::streambuf &buffer, std::string delimiter, ReadCallback callback) = 0;
    virtual void write(const uint8_t *buffer, size_t maxLength, WriteCallback callback) = 0;
    virtual asio::ip::tcp::socket &getSocket() = 0;
    virtual std::string getEndPoint() = 0;
};

class AsioSocket : public IAsioSocket
{
public:
    AsioSocket(IAsioContext::ptr context)
        : IAsioSocket()
        , m_socket(context->getContext()){};

    virtual ~AsioSocket() = default;

    virtual void close() override
    {
        if(m_socket.is_open()){
            m_socket.shutdown(asio::ip::tcp::socket::shutdown_both);
            m_socket.close();
        }
    }

    virtual void connect(const asio::ip::tcp::endpoint &endpoint, ConnectCallback callback) override
    {
        m_socket.async_connect(endpoint, callback);
    }

    virtual void connect(const asio::ip::tcp::resolver::results_type &endpoint, ConnectCallback callback) override
    {
        m_socket.async_connect(*endpoint.cbegin(), callback);
    }

    virtual void read(uint8_t *buffer, size_t maxLength, ReadCallback callback) override
    {
        m_socket.async_read_some(asio::buffer(buffer, maxLength), callback);
    }

    virtual void readUntil(asio::streambuf &buffer, std::string delimiter, ReadCallback callback) override
    {
        asio::async_read_until(m_socket, buffer, delimiter, callback);
    }

    virtual void write(const uint8_t *buffer, size_t maxLength, WriteCallback callback) override
    {
        m_socket.async_write_some(asio::buffer(buffer, maxLength), callback);
    }

    virtual asio::ip::tcp::socket &getSocket() override
    {
        return m_socket;
    }

    virtual std::string getEndPoint() override
    {
        std::stringstream name;
        name << m_socket.remote_endpoint();
        return name.str();
    }

private:
    asio::ip::tcp::socket m_socket;
};