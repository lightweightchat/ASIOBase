#pragma once
#include "tcpConnection.h"
#include <functional>
#include <memory>
#include <string>

#include "Messages.pb.h"

class IResolver
{
public:
    using ptr = std::shared_ptr<IResolver>;
    using DirectHeaderCallback_t = std::function<void(const std::string &, const std::string &, std::vector<uint8_t> &, ITCPConnection::ptr connection)>;
    using BroadcastHeaderCallback_t = std::function<void(const std::string &, std::vector<uint8_t> &, ITCPConnection::ptr connection)>;
    using LoginHeaderCallback_t = std::function<void(const std::string &, const std::string &, ITCPConnection::ptr connection)>;
    using UserConnectionStateChangedCallback_t = std::function<void(const std::string &, bool, ITCPConnection::ptr connection)>;
    using ConnectedUsersHeaderCallback_t = std::function<void(const std::string &, ITCPConnection::ptr connection)>;
    using ServerLogHeaderCallback_t = std::function<void(const std::string &, ITCPConnection::ptr connection)>;


    using KeepAliveHeaderCallback_t = std::function<void(ITCPConnection::ptr connection)>;
    using UndefinedHeaderCallback_t = std::function<void(ITCPConnection::ptr connection)>;

    virtual void setDirectHeaderCallback(DirectHeaderCallback_t callback);
    virtual void setBroadcastHeaderCallback(BroadcastHeaderCallback_t callback);
    virtual void setLoginHeaderCallback(LoginHeaderCallback_t callback);
    virtual void setUserConnectionStateChangedCallback(UserConnectionStateChangedCallback_t callback);
    virtual void setConnectedUsersCallback(ConnectedUsersHeaderCallback_t callback);
    virtual void setServerLogCallback(ServerLogHeaderCallback_t callback);
    virtual void setKeepAliveHeaderCallback(KeepAliveHeaderCallback_t callback);
    virtual void setUndefinedHeaderCallback(UndefinedHeaderCallback_t callback);
    virtual void parseHeader(std::vector<uint8_t> &controlHeader, std::vector<uint8_t> &message, ITCPConnection::ptr connection = nullptr) = 0;

    virtual ~IResolver() = default;

protected:
    DirectHeaderCallback_t dmCallback = nullptr;
    BroadcastHeaderCallback_t broadcastCallback = nullptr;
    LoginHeaderCallback_t loginCallback = nullptr;
    KeepAliveHeaderCallback_t kaCallback = nullptr;
    UndefinedHeaderCallback_t umCallback = nullptr;
    UserConnectionStateChangedCallback_t uConnStateChangedCallback = nullptr;
    ConnectedUsersHeaderCallback_t connectedUsersCallback = nullptr;
    ServerLogHeaderCallback_t serverLogCallback = nullptr;

};

class HeaderResolver : public IResolver
{
public:
    virtual void parseHeader(std::vector<uint8_t> &controlHeader, std::vector<uint8_t> &message, ITCPConnection::ptr connection = nullptr) override;

    // virtual ~HeaderResolver() 
    // {
    //     dmCallback = nullptr;
    //     broadcastCallback = nullptr;
    //     loginCallback = nullptr;
    //     kaCallback = nullptr;
    //     umCallback = nullptr;
    //     uConnStateChangedCallback = nullptr;
    //     connectedUsersCallback = nullptr;
    //     serverLogCallback = nullptr;
    // }

};
