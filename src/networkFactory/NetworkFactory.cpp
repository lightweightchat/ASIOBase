#include "NetworkFactory.h"

IAsioContext::ptr NetworkFactory::makeContext()
{
    return std::make_shared<AsioContext>();
}

IAsioSocket::ptr NetworkFactory::makeSocket(IAsioContext::ptr context)
{
    return std::make_shared<AsioSocket>(context);
}

IAsioAcceptor::ptr NetworkFactory::makeAcceptor(IAsioContext::ptr context, IP version, int port)
{
    return std::make_shared<AsioAcceptor>(context, version, port);
}

ITCPConnection::ptr NetworkFactory::makeTCPConnection(IAsioSocket::ptr socket)
{
    return std::make_shared<TCPConnection>(socket);
}
IAsioResolver::ptr NetworkFactory::makeIpResolver(IAsioContext::ptr context)
{
    return std::make_shared<AsioResolver>(context);
}