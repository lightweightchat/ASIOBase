cmake_minimum_required(VERSION 3.16)

include(CTest)
enable_testing()
include(GoogleTest)

project(Tests)

add_executable(${PROJECT_NAME} 
  ${CMAKE_CURRENT_SOURCE_DIR}/TCPConnectionTests.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/EncryptorTests.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/ProtoResolverTests.cpp
)

target_compile_options(${PROJECT_NAME} PUBLIC ${COMPILE_OPTIONS})

target_include_directories(${PROJECT_NAME} PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}/
  ${CMAKE_CURRENT_SOURCE_DIR}/mocks
)

target_link_libraries(${PROJECT_NAME} PUBLIC
  gtest
  gtest_main
  gmock
  ASIOWrappersMocks
  AES
  source
)

gtest_discover_tests(${PROJECT_NAME})
